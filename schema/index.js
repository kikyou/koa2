import mongoose from 'mongoose';

const { Schema, model } = mongoose;
mongoose.pluralize(null);

const user = new Schema({
  name: String,
  age: Number,
  avatar: String,
  nickname: String,
});
const asoul = model('asoul', user);
export default asoul;

import Koa from 'koa';
import compose from 'koa-compose';
import MD from './middlewares/index.js';
import './config/db.js';

const port = '9000';
const host = '127.0.0.1';
const app = new Koa();

// 加载路由中间件
app.use(compose(MD));

app.on('error', (err, ctx) => {
  if (ctx) {
    ctx.body = {
      code: 9999,
      message: `程序运行时报错：${err.message}`,
    };
  }
});

app.listen(port, host, () => {
  console.log(`API server listening on ${host}:${port}`);
});

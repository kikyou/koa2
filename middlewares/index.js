import logger from 'koa-logger';
import body from 'koa-body';
import conditional from 'koa-conditional-get';
import etag from 'koa-etag';
import koaStatic from 'koa-static';
import cors from '@koa/cors';
import router from '../router/index.js';
import res from './res.js';
import err from './err.js';

const mdCors = cors({
  origin: '*',
  credentials: true,
  allowMethods: ['GET', 'HEAD', 'PUT', 'POST', 'DELETE', 'PATCH'],
});
const mdRoute = router.routes();
const mdRouterAllowed = router.allowedMethods();

export default [
  body(),
  mdCors,
  conditional(),
  etag(),
  koaStatic('./static'),
  res(),
  err(),
  mdRoute,
  mdRouterAllowed,
  logger(),
];

import Router from 'koa-router';
import routes from './routes.js';

const router = new Router();
routes.forEach(item => {
  const { method, path, controller } = item;
  router[method](path, controller);
});
export default router;

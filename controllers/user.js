import asoul from '../schema/index.js';

const user = {
  login: async (ctx, next) => {
    const { name } = ctx.request.body;
    const data = await asoul.findOne({ name }, { nickname: 1, avatar: 1 });
    ctx.set('Content-Type', 'application/json; charset=utf-8');
    ctx.body = data;
    await next();
  },
};
export default user;

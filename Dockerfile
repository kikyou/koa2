FROM registry.gitlab.com/kikyou/sgs/pnpm
RUN mkdir /app
WORKDIR /app
COPY pnpm-lock.yaml ./
RUN pnpm fetch --prod
ADD . ./
RUN pnpm i -r --offline --prod
CMD cd /app && pnpm dev
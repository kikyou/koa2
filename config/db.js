import mongoose from 'mongoose';

mongoose
  .connect('mongodb://127.0.0.1:27017/user')
  .then(() => {
    console.log('数据库连接成功');
  })
  .catch(err => {
    console.log(err);
  });
